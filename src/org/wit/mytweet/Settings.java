package org.wit.mytweet;

import android.app.Activity;
import android.os.Bundle;

public class Settings extends Activity
{

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {

    super.onCreate(savedInstanceState);

    // Checks whether this activity was created previously

    if (savedInstanceState == null)
    {
      SettingsFragment fragment = new SettingsFragment();
      getFragmentManager().beginTransaction().add(android.R.id.content, fragment, fragment.getClass().getSimpleName())
          .commit(); //
    }
  };

}
