package org.wit.mytweet;

import static org.wit.mytweet.helpers.IntentHelper.startActivityWithData;
import static org.wit.mytweet.helpers.IntentHelper.startActivityWithDataForResult;

import java.util.ArrayList;
import java.util.List;

import org.wit.mytweet.helpers.IntentHelper;
import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.models.MyTweet;
import org.wit.mytweet.models.Portfolio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.view.ActionMode;
import android.widget.AbsListView.MultiChoiceModeListener;

public class TimeLineFragment extends ListFragment implements OnItemClickListener, MultiChoiceModeListener
{

  public ArrayList<MyTweet> tweets;
  private ListView listView;
  private TimeLineAdapter adapter;
  private Portfolio portfolio;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {

    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    MyTweetApp app = (MyTweetApp) getActivity().getApplication();
    portfolio = app.portfolio;

    tweets = portfolio.tweets;
    adapter = new TimeLineAdapter(getActivity(), tweets);
    setListAdapter(adapter);

    getActivity().setTitle(R.string.timeline_name);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.timeline, menu);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    View v = super.onCreateView(inflater, parent, savedInstanceState);
    listView = (ListView) v.findViewById(android.R.id.list);
    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    listView.setAdapter(adapter);
    listView.setMultiChoiceModeListener(this);
    return v;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {

    switch (item.getItemId()) //sets the menu items
    {
    case R.id.new_tweet:  //sets the plus bar, which loads tweet page.
      MyTweet tweet = new MyTweet();
      portfolio.newTweet(tweet);

      Intent i = new Intent(getActivity(), TweetPager.class);
      i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet.id);
      startActivityForResult(i, 0);
      return true;

    case R.id.action_settings:
      startActivity(new Intent(getActivity(), Settings.class));
      return true;

    case R.id.clear:
      adapter.clear();
      return true;

    default:
      return super.onOptionsItemSelected(item);
    }

  }

  @Override
  public void onResume()
  {
    super.onResume();
    for (MyTweet tw : tweets)
    {
      if (tw.tweet_text == null || tw.tweet_text.equals(""))
      {
        portfolio.deleteTweet(tw); //deletes tweet if blank.
      }

    }

    adapter.notifyDataSetChanged();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    adapter.notifyDataSetChanged();
  }


  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id)
  {
    MyTweet tweet = adapter.getItem(position);
    IntentHelper.startActivityWithData(getActivity(), TweetPager.class, "TWEET_ID", tweet.id);

  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id)
  {
    MyTweet tweet = ((TimeLineAdapter) getListAdapter()).getItem(position);

    Intent i = new Intent(getActivity(), TweetPager.class);
    i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet.id);
    startActivityForResult(i, 0);
  }

  @Override
  public boolean onActionItemClicked(ActionMode mode, MenuItem item)
  {
    switch (item.getItemId()) //deletes tweet from the list via LongPress.
    {
    case R.id.menu_item_delete_tweet:
      deleteTweet(mode);
      return true;
    default:
      return false;
    }
  }

  @Override
  public boolean onCreateActionMode(ActionMode mode, Menu menu)
  {
    MenuInflater inflater = mode.getMenuInflater();
    inflater.inflate(R.menu.tweet_list_context, menu);
    return true;
  }

  @Override
  public void onDestroyActionMode(ActionMode mode)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean onPrepareActionMode(ActionMode mode, Menu menu)
  {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void onItemCheckedStateChanged(ActionMode arg0, int arg1, long arg2, boolean arg3)
  {
    // TODO Auto-generated method stub

  }

  private void deleteTweet(ActionMode mode)
  {
    for (int i = adapter.getCount() - 1; i >= 0; i--)
    {
      if (listView.isItemChecked(i))
      {
        portfolio.deleteTweet(adapter.getItem(i));
      }
    }
    mode.finish();
    adapter.notifyDataSetChanged();
  }

}

class TimeLineAdapter extends ArrayAdapter<MyTweet>
{
  private Context context;

  public TimeLineAdapter(Context context, ArrayList<MyTweet> tweets)
  {
    super(context, 0, tweets);
    this.context = context;
  }

  @SuppressLint("InflateParams")
  @Override
  public View getView(int position, View convertView, ViewGroup parent)
  {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    if (convertView == null)
    {
      convertView = inflater.inflate(R.layout.list_item_tweet, null);
    }
    MyTweet tweet = getItem(position);

    TextView tweetView = (TextView) convertView.findViewById(R.id.list_item_tweet_text);
    tweetView.setText("" + tweet.tweet_text);

    TextView dateTextView = (TextView) convertView.findViewById(R.id.list_item_tweet_date);
    dateTextView.setText(tweet.getDateString());

    return convertView;
  }
}