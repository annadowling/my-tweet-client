package org.wit.mytweet;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.Menu;

import org.wit.mytweet.R;

public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener
{

  private SharedPreferences prefs;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    addPreferencesFromResource(R.xml.settings_preference); //Loads user inputted preferences from the xml file

    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true); //enables back bar
    getActivity().setTitle(R.string.action_settings);
  }

  @Override
  public void onStart()
  {
    super.onStart();
    prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
    prefs.registerOnSharedPreferenceChangeListener(this);// listens for change in settings preferences.

  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1)
  {
    // TODO Auto-generated method stub

  }

}