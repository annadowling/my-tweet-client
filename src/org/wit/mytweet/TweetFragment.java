package org.wit.mytweet;

import java.util.Date;
import java.util.UUID;
import java.text.DateFormat;

import org.wit.mytweet.R;
import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.models.MyTweet;
import org.wit.mytweet.models.Portfolio;

import android.app.Activity;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;
import static org.wit.mytweet.helpers.IntentHelper.sendEmail;
import static org.wit.mytweet.helpers.ContactHelper.getEmail;
import static org.wit.mytweet.helpers.IntentHelper.selectContact;
import android.content.Intent;

public class TweetFragment extends Fragment implements OnClickListener, TextWatcher
{

  private TextView date;
  private Date current_date;
  private Button tweet;
  private TextView countdown;
  private EditText mind;
  private MyTweet this_tweet;
  private Portfolio portfolio;
  private static final int REQUEST_CONTACT = 1;
  private Button select_contact;
  private Button edit_tweet;

  public static final String EXTRA_TWEET_ID = "mytweet.TWEET_ID";

  @Override
  public void onCreate(Bundle savedInstanceState)
  {

    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    MyTweetApp app = (MyTweetApp) getActivity().getApplication();
    portfolio = app.portfolio;

    UUID tweetId = (UUID) getArguments().getSerializable(EXTRA_TWEET_ID);
    this_tweet = portfolio.getTweet(tweetId);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    addListeners(v);
    updateControls(this_tweet);

    return v;
  }

  private void addListeners(View v)
  {
    date = (TextView) v.findViewById(R.id.date);
    tweet = (Button) v.findViewById(R.id.tweet);
    countdown = (TextView) v.findViewById(R.id.countdown);
    mind = (EditText) v.findViewById(R.id.mind);
    select_contact = (Button) v.findViewById(R.id.select_contact);
    edit_tweet = (Button) v.findViewById(R.id.edit_tweet);

    current_date = new Date();
    date.setText(DateFormat.getDateTimeInstance().format(current_date));
    tweet.setOnClickListener(this);
    select_contact.setOnClickListener(this);
    edit_tweet.setOnClickListener(this);
    mind.addTextChangedListener(this);

  }

  private void updateControls(MyTweet this_tweet2)
  {
    mind.setText(this_tweet2.tweet_text); //Sets the tweet string.
    date.setText(this_tweet2.getDateString()); //Sets the date string.
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {

    int id = item.getItemId();
    if (id == R.id.action_settings)
    {
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onClick(View view) //Controls the tweet page buttons.
  {

    switch (view.getId())
    {

    case R.id.tweet:
      this_tweet.tweet_text = mind.getText().toString();
      Toast toast = Toast.makeText(getActivity(), "Message Sent!", Toast.LENGTH_SHORT);
      toast.show();
      break;

    case R.id.select_contact:
      Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
      startActivityForResult(i, REQUEST_CONTACT);

      break;

    case R.id.edit_tweet:
      sendEmail(getActivity(), select_contact.getText().toString(), getString(R.string.email_report_subject), mind
          .getText().toString());
    }

  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    switch (requestCode)
    {
    case REQUEST_CONTACT:
      if (resultCode != Activity.RESULT_OK)
      {
        return;
      }
      else
        if (requestCode == REQUEST_CONTACT)
        {
          String email = getEmail(getActivity(), data);
          if (this_tweet != null)
          {
            this_tweet.select_contact = email; //Finds contact via email.
          }

          select_contact.setText(email);
        }
      break;
    }
  }

  @Override
  public void afterTextChanged(Editable arg0)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
  {
    // TODO Auto-generated method stub

  }

  @Override
  public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3)
  {
    int c = 140 - mind.length(); //Sets the character count of the tweet
    countdown.setText("" + c);

    if (c == 0)
    {
      Toast.makeText(getActivity().getApplicationContext(), "Maximum Word Limit Reached", Toast.LENGTH_SHORT).show();
    }
  }

  public void onPause()
  {
    super.onPause();
    portfolio.saveTweets();
  }

}
